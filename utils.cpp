//
// Created by firecracker on 17/03/17.
//

#include <fstream>
#include <sys/socket.h>
#include <iostream>
#include <cstring>
#include <array>
#include <ctime>
#include "utils.h"

void send_message(int new_fd, std::string message) {
    long int send_data = send(new_fd, message.c_str(), message.length(), 0);

    // Todo refaktorovat
    while (true) {
        if (send_data == message.length()) break;
        if (send_data == -1){
            std::cerr << "send: " << strerror(errno) << std::endl;
            exit(13);
        }
        long int diff = message.length() - send_data;
        long int new_begining = message.length() - diff;

        send_data = send(new_fd, message.c_str()+new_begining, message.length(), 0);
        if (send_data == diff){
            break;
        }
    }
}

void send_file(std::string filename, int sockfd, bool is_response, std::string status, std::string remote_path) {
    if (remote_path == "") remote_path = filename;
    std::array<char, MAXDATASIZE> buffer;
    std::ifstream data(filename, std::ios::binary | std::ios::ate);
    long long int size = data.tellg();
    data.seekg(std::ios::beg);

    if (is_response){
        send_message(sockfd, generate_resp_header(status));
    } else send_message(sockfd, generate_req_header("PUT", remote_path, "file"));

    std::string header = generate_header(size);
    send_message(sockfd, generate_header(size));

    if (data.is_open()){
        while (!data.eof()){
            data.read(buffer.data(), buffer.size());
            std::string buffer_str(std::begin(buffer), std::begin(buffer)+data.gcount());
            send_message(sockfd, buffer_str);
        }
    } else{
        std::cerr << "client: failed to open file: " << strerror(errno) << std::endl;
        if (is_response) send_message(sockfd, generate_resp_header("404 Not Found"));
    }

}

std::string generate_header(long long int cont_length) {
    std::time_t t = std::time(NULL);
    char mbstr[100];
    std::strftime(mbstr, sizeof(mbstr), "%a, %d %b %Y %T GMT", std::localtime(&t));
    std::string timestamp(mbstr);
    timestamp = "Date: " + timestamp+"\r\n";

    std::string accept("Accept: application/octet-stream\r\n");
    std::string accept_encoding("Accept-Encoding: identity\r\n");
    std::string content_type("Content-Type: application/octet-stream\r\n");
    std::string content_encoding("Content-Encoding: identity\r\n");
    std::string content_length("Content-Length: ");
    content_length = content_length+std::to_string(cont_length)+"\r\n";

    return timestamp+accept+accept_encoding+content_type+content_length+content_encoding+"\r\n";
}

std::string generate_req_header(std::string type, std::string URI, std::string file_or_folder) {
    std::string request = type+" "+URI+"?="+file_or_folder+" "+ " HTTP/1.1 \r\n";
    return request;
}

long long int get_content_length(std::string string){
    unsigned long match = string.find("Content-Length:");
    if (match != std::string::npos) {
        string = string.substr(match+16, string.length()); //ToDo prerobit
        unsigned long match2 = string.find("\r\n");
        if (match2 != std::string::npos) {
            std::string result = string.substr(0, match2);
            return std::stol(result);
        }
    } else return -42;
}

std::string get_request_type(std::string string){
    std::string test = string.substr(0, 3);
    if (string.substr(0, 6) == "DELETE") return "DELETE";
    if (string.substr(0, 3) == "GET") return "GET";
    if (string.substr(0, 3) == "PUT") return "PUT";
    std::cerr << "DELETE/GET/PUT error " << std::endl;
    exit(66);
}

std::string get_first_header_line(std::string string) {
    unsigned long match = string.find("\r\n");
    if (match != std::string::npos) {
        std::string test = string.substr(0, match);
        return string.substr(0, match);
    }
}

std::string get_user_acc(std::string string) {
    unsigned long match = string.find(" ");
    unsigned long match2 = string.find(" HTTP/1.1 \r\n");
    std::string URI = string.substr(match, match2-4);

    unsigned long user_end = URI.substr(1, URI.length()).find("/");
    std::string user = URI.substr(1, user_end);
    return user;
}

std::string get_type(std::string string) {
    unsigned long match = string.find("/");
    unsigned long match2 = string.find(" HTTP/1.1 \r\n");
    std::string URI = string.substr(match, match2-4);

    URI = URI.substr(1, URI.length());
    unsigned long type_pos = URI.find("?=")+2;
    match2 = URI.find(" HTTP/1.1 ");
    std::string type = URI.substr(type_pos, match2-type_pos-1);
    return type;
}

std::string get_path(std::string string) {
    unsigned long match = string.find(get_user_acc(string))+get_user_acc(string).length()+1;
    unsigned long match2 = string.find("?=");
    std::string path = string.substr(match, match2-match);
    return path;
}

std::string generate_resp_header(std::string status_code) {
    std::string request = "HTTP/1.1 "+status_code+"\r\n";
    return request;
}