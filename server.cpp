//
// Created by firecracker on 13/03/17.
//

#include <iostream>
#include <netdb.h>
#include <cstring>
#include <unistd.h>
#include <arpa/inet.h>
#include <ctime>
#include <fstream>
#include "utils.h"
#include <sys/stat.h>
#include <tuple>

std::string PORT = "6677";
std::string LOCAL = ".";

std::string get_command(std::string type, std::string folder_or_file);

void run_command(std::string user, std::string command, std::string path, int i);

void *get_in_addr(sockaddr *sa);

std::tuple<std::string, std::string> get_parameters(int params, char *pString[]);


int main(int argc, char* argv[]) {
    std::string PORT = PORT;
    std::string LOCAL = LOCAL;
    std::tie(LOCAL, PORT) = get_parameters(argc, argv);

    if (chdir(LOCAL.c_str()) != 0){
        std::cerr << "chdir error: " << std::endl;
        exit(59);
    }

    while (true){

        addrinfo hints = {};
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_PASSIVE;

        addrinfo* servinfo;
        int rv = getaddrinfo(nullptr, PORT.c_str(), &hints, &servinfo);
        if (rv != 0) {
            std::cerr << "getaddrifnor: " << gai_strerror(rv) << std::endl;
        }

        int sockfd;
        int yes = 1;
        addrinfo *p;
        for (p = servinfo; p != nullptr; p = p->ai_next) {
            sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);

            if (sockfd == -1) {
                std::cerr << "server: socket" << strerror(errno) << std::endl;
                continue;
            }

            if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1){
                std::cerr << "setsockopt" << strerror(errno) << std::endl;
                exit(42);
            }

            if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1){
                close(sockfd);
                std::cerr << "server: bind" << strerror(errno) << std::endl;
                continue;
            }

            break;
        }

        freeaddrinfo(servinfo);

        if (!p) {
            std::cerr << "server: failed to bind " << strerror(errno) << std::endl;
            exit(42);
        }

        if (listen(sockfd, SOMAXCONN) == -1) {
            std::cerr << "listen" << strerror(errno) << std::endl;
            exit(42);
        }


        std::cout << "Waiting for connections..." << std::endl;

        socklen_t sin_size;
        sockaddr_storage their_addr;
        int new_fd;
        char str[INET6_ADDRSTRLEN];
        while (true) {
            sin_size = sizeof their_addr;
            new_fd = accept(sockfd, (sockaddr *) &their_addr, &sin_size);

            if (new_fd == -1) {
                std::cerr << "accept" << strerror(errno) << std::endl;
                continue;
            }

            inet_ntop(their_addr.ss_family, get_in_addr((sockaddr *) &their_addr), str, sizeof str);
            std::cout << "Connection from " << str << " established!" << std::endl;


            close(sockfd);

            std::cout << "Client " << str << " send message" << std::endl;

            char buf[MAXDATASIZE];
            long int numbytes;

            std::ofstream ostrm;
            std::string raw_data;
            unsigned long data_len = 0;
            std::string header;
            bool header_found = false;
            std::string req_type = "";
            std::string command = "";
            std::string folder_or_file = "";
            std::string user = "";
            std::string path = "";
            while (true) {
                numbytes = recv(new_fd, buf, MAXDATASIZE, 0);
                if (numbytes == -1) {
                    std::cerr << "recieve " << strerror(errno) << std::endl;
                    break;
                }

                std::string msg(buf, numbytes);
                raw_data = raw_data + msg;
                unsigned long pos = raw_data.find("\r\n\r\n");
                std::string data;
                if (pos != std::string::npos) {
                    header_found = true;
                    header = raw_data.substr(0, pos);
                    data = raw_data.substr(pos + 4, raw_data.size());
                    data_len = data.length();
                    raw_data = "";
                    req_type = get_request_type(header);
                    folder_or_file = get_type(header);
                    user = get_user_acc(header);
                    command = get_command(req_type, folder_or_file);
                    path = get_path(header);
                    if (command == "put") {
                        ostrm.open(path, std::ios::binary);
                        if (!ostrm.is_open()){
                            send_message(new_fd, generate_resp_header("400 Bad Request"));
                            send_message(new_fd, generate_header(0));
                            break;
                        }
                        ostrm.write(data.c_str(), data.length());
                    } else
                        run_command(user, command, path, new_fd);
                    if (data_len == get_content_length(header)) {
                        break;
                    }
                    continue;
                }

                if (header_found) {
                    data = data + raw_data;
                    data_len = data_len + data.length();
                    if (command == "put") {
                        ostrm.write(data.c_str(), data.length());
                    } else
                        run_command(user, command, path, new_fd);
                    raw_data = "";
                }

                if (data_len == get_content_length(header)) {
                    break;
                }
            }
            if (command == "put") {
                send_message(new_fd, generate_resp_header("200 OK"));
                send_message(new_fd, generate_header(0));
            }
            ostrm.close();
            close(new_fd);
            std::cout << "Closing socket" << std::endl;
            break;
        }
    }
}

void run_command(std::string user, std::string command, std::string path, int i) {
    if (command == "del"){
        if( remove(path.c_str()) != 0 ){
            send_message(i, generate_resp_header("400 Bad Request"));
            send_message(i, generate_header(0));
            return;
        } else {
            send_message(i, generate_resp_header("200 OK"));
            send_message(i, generate_header(0));
            return;
        }
    }
    if (command == "get"){
        send_file(path, i, true);
        return;
    }
    if (command == "lst"){
        std::ifstream test(path);
        if (!test.is_open()){
            send_message(i, generate_resp_header("400 Bad Request"));
            send_message(i, generate_header(0));
            return;
        }

        std::string lscomm = "ls "+path+">lsout.txt";
        if (std::system(lscomm.c_str()) != 0) {
            send_message(i, generate_resp_header("404 Not Found"));
            send_message(i, generate_header(0));
            return;
        }

        send_file("lsout.txt", i, true);
        return;
    }
    if (command == "mkd"){
        if (mkdir(path.c_str(), 0655) != 0) {
            send_message(i, generate_resp_header("400 Bad Request"));
            send_message(i, generate_header(0));
            return;
        } else{
            send_message(i, generate_resp_header("200 OK"));
            send_message(i, generate_header(0));
            return;;
        }
    }
    if (command == "rmd"){
        if (rmdir(path.c_str()) != 0){
            send_message(i, generate_resp_header("400 Bad Request"));
            send_message(i, generate_header(0));
            return;
        } else{
            send_message(i, generate_resp_header("200 OK"));
            send_message(i, generate_header(0));
            return;;
        }
    }
}

std::string get_command(std::string req_type, std::string folder_or_file) {
    if (req_type == "GET"){
        if (folder_or_file == "folder") return "lst";
        if (folder_or_file == "file") return "get";
    }
    if (req_type == "PUT"){
        if (folder_or_file == "folder") return "mkd";
        if (folder_or_file == "file") return "put";
    }
    if (req_type == "DELETE"){
        if (folder_or_file == "folder") return "rmd";
        if (folder_or_file == "file") return "del";
    }
}

void *get_in_addr(sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((sockaddr_in*)sa) -> sin_addr);
    }
    return &(((sockaddr_in6*)sa) -> sin6_addr);
}

std::tuple<std::string, std::string> get_parameters(int params, char **pString) {
    std::string remote = ".";
    std::string port = PORT;
    int c;
    while ((c = getopt(params, pString, "r:p:")) != -1){
        switch (c){
            case 'r':
                remote = optarg;
                break;
            case 'p':
                port = optarg;
                break;
            case '?':
                std::cerr << "parameter error: " << c << std::endl;
                exit(42);
            default:
                break;
        }
    }
    return std::make_tuple(remote, port);
}

///std::tie(a,b) =