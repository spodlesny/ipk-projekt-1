//
// Created by firecracker on 13/03/17.
//


#include <iostream>
#include <netdb.h>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include <wait.h>
#include <arpa/inet.h>
#include <fstream>
#include <vector>
#include <array>
#include "utils.h"
#include <algorithm>

std::string PORT = "8000";

void check_parameters(int params, char **pString);

void *get_in_addr(sockaddr* sa);

std::string get_local_path(int argc, char **pString);
std::string get_remote_path(int argc, char **pString);
std::string get_remote_server(int argc, char **pString);
std::string get_remote_port(int argc, char **pString);
std::string get_command(int argc, char **pString);


void run_resp_command(std::string command, std::string local_path, std::string string, std::string header);
void run_command(int sockfd, std::string command, std::string local_path, std::string remote_path);

std::string get_resp_code(std::string header);

int main(int argc, char* argv[]){
    check_parameters(argc, argv);

    addrinfo hints = {};
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;



    std::string server = get_remote_server(argc, argv);
    PORT = get_remote_port(argc, argv);
    addrinfo *servinfo;
    int rv = getaddrinfo(server.c_str(), PORT.c_str(), &hints, &servinfo);
    if (rv != 0) {
        std::cerr << "getaddrifnor: " << gai_strerror(rv) << std::endl;
        return 42;
    }

    addrinfo *p;
    int sockfd;
    for (p = servinfo; p != 0 ; p = p->ai_next) {
        sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (sockfd == -1){
            std::cerr << "client: socket " << strerror(errno) << std::endl;
            continue;
        }

        int status = connect(sockfd, p->ai_addr, p->ai_addrlen);
        if (status == -1){
            close(sockfd);
            std::cerr << "client: connect " << strerror(errno) << std::endl;
            continue;
        }

        break;
    }

    if (p == nullptr){
        std::cerr << "client: failed to connect " << strerror(errno) << std::endl;
        exit(47);
    }

    char str[INET6_ADDRSTRLEN];
    inet_ntop(p->ai_family, get_in_addr((sockaddr *)p->ai_addr), str, sizeof str);

    std::cout << "Connecting to: " << str << std::endl;

    freeaddrinfo(servinfo);

    //send_file(local_path, sockfd, false, "200 OK",remote_path);
    //close(sockfd); //ToDo presunut na spodok
    //send_message(sockfd, "test");
    //std::cout << "Data sent" << std::endl;
    std::string command = get_command(argc, argv);
    std::string remote_path = get_remote_path(argc, argv); //ToDo presunut potom ked pouzijem
    std::string local_path = get_local_path(argc, argv);
    run_command(sockfd, command, local_path, remote_path);

    //ToDo do while cyklu
    std::string raw_data;
    unsigned long data_len = 0;
    std::string header;
    bool header_found = false;
    std::ofstream ostrm;
    std::string data;
    while (true){
        char buf[MAXDATASIZE];
        long int numbytes = recv(sockfd, buf, MAXDATASIZE, 0);
        if (numbytes == -1) {
            std::cerr << "recv " << strerror(errno) << std::endl;
            break;
        }

        std::string msg(buf, numbytes);
        raw_data = raw_data + msg;
        unsigned long pos = raw_data.find("\r\n\r\n");
        if (pos != std::string::npos) {
            header_found = true;
            header = raw_data.substr(0, pos);
            data = raw_data.substr(pos + 4, raw_data.size());
            data_len = data.length();
            raw_data = "";
            if (command == "get") {
                std::string resp_code = get_resp_code(get_first_header_line(header));
                if (resp_code != "200 OK"){
                    std::cerr << "File not found." << std::endl;
                    exit(6);
                }
                ostrm.open(local_path, std::ios::binary);
                if (!ostrm.is_open()){
                    std::cerr << "Not a file." << std::endl;
                    break;
                }
                ostrm.write(data.c_str(), data.length());
            } else {
                run_resp_command(command, local_path, data, header);
            }

            std::cout << "Client recieved message: " << msg << std::endl;
            if (data_len == get_content_length(header)) {
                break;
            }
            continue;
        }

        if (header_found) {
            data = data + raw_data;
            data_len = data_len + data.length();
            if (command == "get") {
                ostrm.write(data.c_str(), data.length());
            } else {
                run_resp_command(command, local_path, data, header);
            }
            raw_data = "";
        }

        std::cout << "Client recieved message: " << msg << std::endl;
        if (msg == ""){
            break;
        }

        if (data_len == get_content_length(header)) {
            break;
        }

        //break; //todo tmp

    }
    close(sockfd);
    return 0;
}

void *get_in_addr(sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((sockaddr_in*)sa) -> sin_addr);
    }
    return &(((sockaddr_in6*)sa) -> sin6_addr);
}

void check_parameters(int params, char **pString) {
    if (params == 1){
        std::cerr << "maybe help later?" << std::endl; //ToDo
        exit(42);
    }

    std::vector<std::string> parameters = {"del", "get", "put", "lst", "mkd", "rmd"};
    std::string command(pString[1]);
    if (std::find(std::begin(parameters), std::end(parameters), command) == std::end(parameters)){
        std::cerr << "command error: unknow command " << command << std::endl;
        exit(42);
    }

    if (params == 2) {
        std::cerr << "parameter error: REMOTE-PATH was not set" << std::endl;
        exit(42);
    }

    if (params == 3) {
        if (command == "put"){
            std::cerr << "parameter error: command put must be run with LOCAL-PATH" << std::endl;
            exit(42);
        }
        return;
    }

    if (params == 4) {
        return;
    }

    std::cerr << "parameter error: too many parameters was set" << std::endl;
    exit(42);

}

std::string get_local_path(int argc, char **pString) {
    if (argc == 3){
        return ".";
    }
    return std::string(pString[3]);
}

//"http://localhost/lorem.txt"
std::string get_remote_path(int argc, char **pString) {
    std::string rd = pString[2];
    rd = rd.substr(7, rd.length());
    unsigned long match = rd.find("/");
    return rd.substr(match+1, rd.length());
}

std::string get_remote_server(int argc, char **pString) {
    std::string rd = pString[2];
    rd = rd.substr(7, rd.length());
    unsigned long match = rd.find(":");
    std::string test = rd.substr(0, match);
    return test;
}

std::string get_remote_port(int argc, char **pString) {
    std::string rd = pString[2];
    rd = rd.substr(7, rd.length());
    unsigned long match = rd.find(":");
    unsigned long match2 = rd.find("/");
    return rd.substr(match+1, match2-match-1);
}

std::string get_command(int argc, char **pString) {
    return pString[1];
}

void run_command(int sockfd, std::string command, std::string local_path, std::string remote_path) {
    if (command == "del"){
        send_message(sockfd, generate_req_header("DELETE", remote_path, "file"));
        send_message(sockfd, generate_header(0));
    }
    if (command == "get"){
        send_message(sockfd, generate_req_header("GET", remote_path, "file"));
        send_message(sockfd, generate_header(0));
    }
    if (command == "put"){
        send_message(sockfd, generate_req_header("PUT", remote_path, "file"));
        send_file(local_path, sockfd, false, "", remote_path);
    }
    if (command == "lst"){
        send_message(sockfd, generate_req_header("GET", remote_path, "folder"));
        send_message(sockfd, generate_header(0));
    }
    if (command == "mkd"){
        send_message(sockfd, generate_req_header("PUT", remote_path, "folder"));
        send_message(sockfd, generate_header(0));
    }
    if (command == "rmd"){
        send_message(sockfd, generate_req_header("DELETE", remote_path, "folder"));
        send_message(sockfd, generate_header(0));
    }
}

void run_resp_command(std::string command, std::string local_path, std::string data, std::string header) {
    std::string resp_code = get_resp_code(get_first_header_line(header));
    if (command == "del"){
        if (resp_code == "200 OK"){
            return;
        }
        if (resp_code == "400 Bad Request"){
            //Todo: File not found.
            std::cerr << "Not a file." << std::endl;
            exit(5);
        }

    }
    if (command == "put"){
        if (resp_code == "200 OK") {
            return;
        }
        if (resp_code == "400 Bad Request") {
            std::cerr << "Already exists." << std::endl;
            exit(4);
        }
    }
    if (command == "lst"){
        if (resp_code == "200 OK") {
            std::cout << data << std::endl;
            return;
        }
        if (resp_code != "200 OK") {
            std::cerr << "Directory not found." << std::endl;
            exit(2);
        }
    }
    if (command == "mkd"){
        if (resp_code == "200 OK") {
            return;
        }
        if (resp_code == "400 Bad Request") {
            std::cerr << "Already exists." << std::endl;
            exit(4);
        }
    }
    if (command == "rmd"){
        if (resp_code == "200 OK") {
            return;
        }
        if (resp_code == "400 Bad Request") {
            std::cerr << "Directory not found." << std::endl;
            exit(2);
        }
    }
}

std::string get_resp_code(std::string header) {
    return header.substr(9, header.length());
}

