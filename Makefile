CXXFLAGS+=-Wall -Wextra -std=c++11 -g

.PHONY: all
all: ftrest ftrestd

ftrest: client.cpp utils.o
	$(CXX) $(CXXFLAGS) -o $@ $^

ftrestd: server.cpp utils.o
	$(CXX) $(CXXFLAGS) -o $@ $^

utils.o: utils.cpp utils.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

.PHONY: clean
clean:
	rm ftrest ftrestd utils.o