## NAME
ftrest - client application for connecting to server
server - create basic RESTful server
## SYNOPSIS
`ftrest COMMAND REMOTE-PATH [LOCAL-PATH]`
`ftrestd [-r ROOT-FOLDER] [-p PORT]`
## DESCRIPTION
***ftrest*** support following **COMMAND**s:
**del** - remove file specified by **REMOTE-PATH**
**get** - copy file from **REMOTE-PATH** to current directory or place specified by  **LOCAL-PATH** if is defined
**put** - copy file from  **LOCAL-PATH** into  **REMOTE-PATH** directory
**lst** - list content of remote directory on stdout
**mkd** - create directory specified by  **REMOTE-PATH** on server
**rmd** - remove directory specified by  **REMOTE-PATH** on server

**LOCAL-PATH** must be define only if **put** is called and will specified route to local file system
***REMOTE-PATH*** is route file or directory on server

***ftrestd*** have to optional switches:
**-r ROOT-FOLDER**  - specifying root directory where files will be stored. Default value is current working directory
***-p PORT*** specifying port where server will listen. Default value is 6677

### Exit status:
**1** Not a directory
**2** Directory not found
**3** Directory not empty
**4** Already exists
**5** Not a file
**6** File not found
**7** User Account Not Found
**8 - 99** Unknown error

## EXAMPLES
Create directory on server
$ ftrest mkd http://localhost:12345/tonda/foo/bar

Upload file on server to directory bar
$ ftrest put http://localhost:12345/tonda/foo/bar/doc.pdf ~/doc.pdf

Download file doc.pdf into local directory
$ ftrest get http://localhost:12345/tonda/foo/bar/doc.pdf

Remove file doc.pdf from server
$ ftrest del http://localhost:12345/tonda/foo/bar/doc.pdf

Remove bar/ directory
$ ftrest rmd http://localhost:12345/tonda/foo/bar

## AUTHOR
Simon Podlesny - spodlesny@gmail.com