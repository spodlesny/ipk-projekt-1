//
// Created by firecracker on 17/03/17.
//

#ifndef IPK_UTILS_H
#define IPK_UTILS_H

const int MAXDATASIZE = 4096;
void send_message(int new_fd, std::string message);
void send_file(std::string filename, int sockfd, bool is_response = false, std::string status = "200 OK", std::string remote_path = "");
std::string generate_req_header(std::string type, std::string URI, std::string file_or_folder);
std::string generate_resp_header(std::string status_code);
std::string generate_header(long long int cont_length);
long long int get_content_length(std::string string);
std::string get_request_type(std::string string);
std::string get_first_header_line(std::string string);
std::string get_user_acc(std::string string);
std::string get_type(std::string string); //Folder or File
std::string get_path(std::string string); //Folder or File
#endif //IPK_UTILS_H
